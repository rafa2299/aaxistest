<?php

namespace App\Service;
use App\Entity\Dto\ResponseDTO;


/**
 * Class IProductService
 */
interface IProductService
{
    public function getAll(): ResponseDTO;
    public  function getById(string $id): ResponseDTO;
    public function saveProduct(array $params): ResponseDTO;
    public function updateProduct(array $params): ResponseDTO;
}
