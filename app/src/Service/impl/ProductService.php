<?php

namespace App\Service\impl;

use App\Entity\Dto\ResponseDTO;
use App\Entity\Product;
use App\Service\IProductService;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductService implements IProductService
{

    /**
     * @var ProductRepository $productRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Return all products
     *
     * @return ResponseDTO
     */
    public function getAll(): ResponseDTO
    {
        /** @var ResponseDTO $response */
        $response = new ResponseDTO();
        $response->setStatus(Response::HTTP_OK);
        $response->setData($this->productRepository->findAll());
        return $response;
    }
    /**
     * Return product from id
     */
    public function getById(string $id): ResponseDTO
    {
        /** @var ResponseDTO $response */
        $response = new ResponseDTO();
        $response->setStatus(Response::HTTP_OK);
        $response->setData($this->productRepository->findById($id));
        return $response;
    }

    /**
     * Save product
     *
     * @param array $params
     * @return ResponseDTO
     */
    public function saveProduct(array $params): ResponseDTO
    {
        /** @var ResponseDTO $response */
        $response = new ResponseDTO();
        $this->validatePost($params);
        $product = $this->productRepository->save($this->getProductFromParams($params));
        $this->productRepository->flush();
        $response->setMessage(sprintf("Product with sku %s created succesfully", $params['sku']));
        $response->setStatus(Response::HTTP_CREATED);
        $response->setData($product);
        return $response;
    }

    /**
     * Update product from params
     *
     * @param array $params
     * @return ResponseDTO
     */
    public function updateProduct(array $params): ResponseDTO
    {
        $product = null;
        $responseList = new ArrayCollection();
        foreach ($params as $item) {
            $sku = $item['sku'];
            /** @var ResponseDTO $response */
            $response = new ResponseDTO();
            try {
                $this->validate($item);
                /** @var Product $product */
                $product = $this->getBySku($sku);
                $this->updateProductFromParams($product, $item);
                $product = $this->productRepository->save($product);
                $this->productRepository->flush();
                $response->setMessage(sprintf("Product with sku %s updated succesfully", $sku));
                $response->setStatus(Response::HTTP_OK);
            } catch (HttpException $e) {
                $response->setMessage($e->getMessage());
                $response->setStatus($e->getStatusCode());
            }
            $response->setData($item);
            $responseList->add($response);
        }
        $response = new ResponseDTO();
        $response->setStatus(Response::HTTP_ACCEPTED);
        $response->setData($responseList);
        return $response;
    }

    /**
     * Return by sku
     *
     * @param [type] $sku
     * @return void
     */
    private function getBySku($sku): Product
    {
        $product = $this->productRepository->findOneBySku($sku);
        if (is_null($product)) {
            throw new NotFoundHttpException(sprintf("Product not found withs sku %s", $sku));
        }
        return $product;
    }

    /**
     * Validate post
     *
     * @param array $item
     * @return void
     */
    private function validatePost(array $item)
    {
        try {
            $this->getBySku($item['sku']);
            throw new BadRequestHttpException(sprintf("Product with sku %s already exist", $item['sku']));
        } catch (NotFoundHttpException $e) {
            $this->validate($item);
        }
    }
    /**
     * Validate basic
     */
    private function validate(array $item)
    {
        if (!isset($item['sku'])) {
            throw new BadRequestHttpException(sprintf("Field sku is required"));
        }
        if (is_null($item['sku'])) {
            throw new BadRequestHttpException(sprintf("Field sku is null"));
        }
        if (!isset($item['product_name'])) {
            throw new BadRequestHttpException(sprintf("Field product_name is required - sku %s", $item['sku']));
        }
        if (is_null($item['product_name'])) {
            throw new BadRequestHttpException(sprintf("Field product_name is null - sku %s", $item['sku']));
        }
        if (!isset($item['description'])) {
            throw new BadRequestHttpException(sprintf("Field description is required - sku %s", $item['sku']));
        }
        if (is_null($item['description'])) {
            throw new BadRequestHttpException(sprintf("Field description is null - sku %s", $item['sku']));
        }
    }

    /**
     * Update from params
     */
    private function updateProductFromParams(Product &$product, array $item)
    {
        $product->setProductName($item['product_name']);
        $product->setDescription($item['description']);
    }

    /**
     * Returns entity product from array params
     */
    private function getProductFromParams(array $params): Product
    {
        $product = new Product();
        $product->setSku($params['sku']);
        $product->setProductName($params['product_name']);
        $product->setDescription($params['description']);
        $product->setCreatedAt(new \DateTimeImmutable());
        return $product;
    }
}
