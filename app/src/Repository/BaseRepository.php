<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class BaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, string $class)
    {
        parent::__construct($registry, $class);
    }
    /**
     * Persist data
     *
     * @param null|object $entity
     *
     * @return Object
     */
    public function save(object|null $entity): mixed
    {
        $this->getEntityManager()->persist($entity);
        return $entity;
    }
    /**
     * Delete 
     * @param $entity
     */
    public function delete(object|null $entity)
    {
        $this->getEntityManager()->remove($entity);
    }

    /**
     * Flush
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * Begin Transaction
     */
    public function beginTransaction()
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
    }

    public function isTransactionActive()
    {
        $this->getEntityManager()->getConnection()->isTransactionActive();
    }

    /**
     * Commit
     */
    public function commit()
    {
        $this->getEntityManager()->getConnection()->commit();
    }

    /**
     * Rollback
     */
    public function rollback()
    {
        $this->getEntityManager()->getConnection()->rollBack();
    }
}
