<?php

namespace App\Controller;

use App\Entity\Dto\ResponseDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BaseController extends AbstractController
{
    protected $response;
    protected $serializer;

    public function __construct()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }
    /**
     * Return response dto
     *
     * @param mixed $result
     * @param string|null $message
     * @param integer $status
     * @return JsonResponse
     */
    protected function getResponse(mixed $result, string $message = null, int $status=200): JsonResponse
    {
        if ($result instanceof ResponseDTO) {
            return $this->json($result, $status);
        }
        $this->response = new ResponseDTO();
        $this->response->setData($result);
        $this->response->setMessage($message);
        return $this->json($this->response, $status);
    }
}
