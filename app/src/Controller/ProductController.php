<?php

namespace App\Controller;

use App\Entity\Dto\ResponseDTO;
use App\Service\impl\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

#[Route("/api/v1.0/product", "api_")]
class ProductController extends BaseController
{

    public function __construct(
        private ProductService $productService
    ) {
    }

    /**
     * Return product by id
     */
    #[Route("/{id}", "get_product", methods: ["GET"])]
    public function getProductById(string $id): JsonResponse
    {
        return $this->getResponse($this->productService->getById($id));
    }

    /**
     * Returns all product
     *
     * @return JsonResponse
     */
    #[Route("", "get_all_product", methods: ["GET"])]
    public function getAllProduct(): JsonResponse
    {
        return $this->getResponse($this->productService->getAll());
    }

    /**
     * Save product
     *
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('', name: 'post_product', methods: ["POST"])]
    public function postProduct(Request $request): JsonResponse
    {
        $params = json_decode($request->getContent(), true);
        return $this->getResponse($this->productService->saveProduct($params));
    }

    /**
     * Update product
     *
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('', name: 'put_product', methods: ["PUT"])]
    public function putProduct(Request $request): JsonResponse
    {
        $params = json_decode($request->getContent(), true);
        return $this->getResponse($this->productService->updateProduct($params));
    }
}
