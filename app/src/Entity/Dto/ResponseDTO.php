<?php

namespace App\Entity\Dto;


class ResponseDTO
{
    private $message;
    private $data;
    private $status;




    /**
     * Get message
     */
    public function getMessage()
    {
        return $this->message;
    }

   /**
    * Set message
    */
    public function setMessage(string|null $message)
    {
        $this->message = $message;
    }

    /**
     * Get data
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data
     */
    public function setData(mixed $data)
    {
        $this->data = $data;
    }
    /**
     * Get status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     */
    public function setStatus(int $data)
    {
        $this->status = $data;
    }
}
