# AaxisTest



## Getting started

To tun project execute

```bash
docker-compose up -d --force-recreate --build
```

Acces to container
```bash
docker exec -it php_aaxistest bash
```

Into container execute composer install

```bash
root@37a1123e74d1:/var/www/aaxistest# composer install
```

Execute migrations to create table Product
```bash
root@37a1123e74d1:/var/www/aaxistest# php bin/console doctrine:migrations:migrate
```

After create key par
```bash
php bin/console lexik:jwt:generate-keypair
```

## Endpoints
### Login
```bash
curl --location --request GET 'http://localhost:8089/api/login_check' \
--header 'Content-Type: application/json' \
--data-raw '{
"username":"admin",
"password":"admin"
}'
```
This endopoint return token for consume endpoints
### Get all

Example
```bash
curl --location --request GET 'http://localhost:8089/api/v1.0/product' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTg4MDQ4MjAsImV4cCI6MTcxODgwODQyMCwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluIn0.9CJcue2-SjWIWT_NEJJEg5WHZ5r5y3_csw2F1gW6TB1hqiOG4YrLb6XJ7BP9QVlXEz9i1aNwix5PMo_UcQQ5taOzcbeotjAArbh1SZlxbZTHalKHKOIVDvKyiPBJUHu0w42AlSJWDkqqfOQdvaJFZhp2T_yDdKgFokYAQVN34eGmeiELswva2p3RCBmlPG00A6ReSHNXj2X8nrbmocsPdxxHn2WYxkg2E_E4-53py3wGf20BYsx2AUQbY88y0gjrwM_fZXKWnRXT1LaCLqc_ogo8DFiW5mZlaDS7LqtTVs1EsV1wONQaKK_vlkpIYOS8dhg3VMb1ASNUxT7rwsJE2g'
```
### Add product
Example
```bash
curl --location --request POST 'http://localhost:8089/api/v1.0/product' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTg4MDcwNDgsImV4cCI6MTcxODgxMDY0OCwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluIn0.KYJgdrszBGT7X7MotqortoBkJ0cH7uy7EwTCarFeQMGnzX3l1Nc7bBrR0mSjHSG-2EorlmoGB_wwCcEJB50tGyO73CeNexMZwyLayRjLrB-VTKB4jOnW2PbEJ4BgjID4Rw6lKktyJrHiA20LpbaU31A-al9MkwHIxOWB9Zoj5eCeFZtu2cfeCHLviq9DCV5cLtzH8Gjb5Mg5hCY4LuFWqfHeikf20CKtKQFc5G9ArVzGkk5I8oXSazJUjBQwLYBBV2wFc82x817qFtWtg_389o0QR7tBwox1w5o3SuSmAQfdHck4uKepz2b2xItblTJDPPxjIKE7hW_xaIsR4svfQQ' \
--header 'Content-Type: application/json' \
--data-raw '{
    "sku":"78975454",
    "product_name":"productName",
    "description":"description"
}'
```
### Update product
```bash
curl --location --request PUT 'http://localhost:8089/api/v1.0/product' \
--header 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MTg4MDQ4MjAsImV4cCI6MTcxODgwODQyMCwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6ImFkbWluIn0.9CJcue2-SjWIWT_NEJJEg5WHZ5r5y3_csw2F1gW6TB1hqiOG4YrLb6XJ7BP9QVlXEz9i1aNwix5PMo_UcQQ5taOzcbeotjAArbh1SZlxbZTHalKHKOIVDvKyiPBJUHu0w42AlSJWDkqqfOQdvaJFZhp2T_yDdKgFokYAQVN34eGmeiELswva2p3RCBmlPG00A6ReSHNXj2X8nrbmocsPdxxHn2WYxkg2E_E4-53py3wGf20BYsx2AUQbY88y0gjrwM_fZXKWnRXT1LaCLqc_ogo8DFiW5mZlaDS7LqtTVs1EsV1wONQaKK_vlkpIYOS8dhg3VMb1ASNUxT7rwsJE2g' \
--header 'Content-Type: application/json' \
--data-raw '[
    {
     "sku":"878787",
     "product_name":"coca fanta222wwwww2",
     "description":"fantaaaa"
    },
    {
     "sku":"144444",
     "product_name":"coca fanta2222",
     "description":"fantaaaa"
    }
]
```
